## Overview
Code samples (mainly Jupyter notebooks) for internal use in Aoki Lab in National Institute for Basic Biology.
The samples are classified into directories
based on the contents, e.g., model simulation, image analysis, etc.

To render Jupyter notebooks:
![how_to_render_notebook](viewer.png)

## Requirement
Anaconda3 (https://www.continuum.io/downloads)